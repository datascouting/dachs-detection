package com.datascouting.dachs.detection.dto;

import org.intellift.sol.domain.Identifiable;

public abstract class AbstractDto implements Identifiable<String> {

    protected String id;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }
}
