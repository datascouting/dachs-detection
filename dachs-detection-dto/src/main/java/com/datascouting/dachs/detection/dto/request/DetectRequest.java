package com.datascouting.dachs.detection.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DetectRequest {

    @ApiModelProperty(value = "Raw text for detection")
    private String text;

    @ApiModelProperty(value = "Languages in which the detection will be performed")
    private Set<String> languages;

    @ApiModelProperty(value = "Perform hate speech detection")
    private Boolean hateSpeechDetection;

    @ApiModelProperty(value = "Perform attack detection")
    private Boolean personalAttackDetection;

    @ApiModelProperty(value = "Perform semantic analysis")
    private Boolean semanticAnalysis;

    @ApiModelProperty(value = "The id that correlates the asynchronous processing request to the response", allowEmptyValue = true)
    private String correlationId;

    public static DetectRequestBuilder builder(String text) {
        return new DetectRequestBuilder()
                .text(text);
    }
}
