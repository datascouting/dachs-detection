package com.datascouting.dachs.detection.dto.response;

import com.datascouting.dachs.detection.dto.UserDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TokenResponse {

    @ApiModelProperty(value = "Published token")
    private String token;

    @ApiModelProperty(value = "Current logged in user")
    private UserDto user;
}
