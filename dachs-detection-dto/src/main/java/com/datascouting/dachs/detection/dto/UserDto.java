package com.datascouting.dachs.detection.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class UserDto extends AbstractDto {

    private String username;
    @JsonIgnore
    private String password;
    private String email;
    private String name;
    private Boolean enabled;
    private List<String> roles = new ArrayList<>();
}
