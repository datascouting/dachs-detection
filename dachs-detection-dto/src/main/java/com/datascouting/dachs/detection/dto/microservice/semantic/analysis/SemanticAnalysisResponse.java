package com.datascouting.dachs.detection.dto.microservice.semantic.analysis;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

public class SemanticAnalysisResponse extends ArrayList<SemanticAnalysisResponse.Token> {

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class Token {

        @ApiModelProperty(value = "Token text")
        private String text;
        @ApiModelProperty(value = "Token's classification tag")
        private String tag;
        @ApiModelProperty(value = "Named Entity Recognition")
        private String ner;
        @ApiModelProperty(value = "Part of Speech")
        private String pos;
    }
}
