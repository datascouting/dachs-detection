package com.datascouting.dachs.detection.dto.microservice.hate.speech.detection;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HateSpeechDetectionRequest {

    private String text;
    private String language;
}
