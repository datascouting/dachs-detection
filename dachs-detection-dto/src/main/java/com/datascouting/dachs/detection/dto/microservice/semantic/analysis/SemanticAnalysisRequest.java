package com.datascouting.dachs.detection.dto.microservice.semantic.analysis;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SemanticAnalysisRequest {

    private String language;
    private String text;
}
