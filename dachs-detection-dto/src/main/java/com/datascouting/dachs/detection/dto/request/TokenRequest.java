package com.datascouting.dachs.detection.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenRequest {

    @ApiModelProperty(value = "Account username")
    private String username;

    @ApiModelProperty(value = "Account password")
    private String password;
}
