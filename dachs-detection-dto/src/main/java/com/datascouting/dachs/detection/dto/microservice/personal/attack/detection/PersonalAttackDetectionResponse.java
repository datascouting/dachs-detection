package com.datascouting.dachs.detection.dto.microservice.personal.attack.detection;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PersonalAttackDetectionResponse {

    private Double probability;
    private String status;
}
