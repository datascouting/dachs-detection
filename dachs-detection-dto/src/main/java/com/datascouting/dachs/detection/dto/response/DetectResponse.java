package com.datascouting.dachs.detection.dto.response;

import com.datascouting.dachs.detection.dto.DetectionDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DetectResponse {

    @ApiModelProperty(value = "The id that correlates the asynchronous processing request to the response")
    private String correlationId;

    @ApiModelProperty(value = "Detection result")
    private DetectionDto detection;
}
