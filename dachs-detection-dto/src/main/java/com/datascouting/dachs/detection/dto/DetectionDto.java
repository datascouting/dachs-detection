package com.datascouting.dachs.detection.dto;

import com.datascouting.dachs.detection.dto.microservice.hate.speech.detection.HateSpeechDetectionResponse;
import com.datascouting.dachs.detection.dto.microservice.personal.attack.detection.PersonalAttackDetectionResponse;
import com.datascouting.dachs.detection.dto.microservice.semantic.analysis.SemanticAnalysisResponse;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DetectionDto {

    @ApiModelProperty(value = "Detection results for every requested language")
    private Set<LanguageDetection> languageDetections;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LanguageDetection {

        @ApiModelProperty(value = "Requested language", required = true)
        private String language;

        @ApiModelProperty(value = "Hate Speech Detection")
        private HateSpeechDetectionResponse hateSpeechDetection;

        @ApiModelProperty(value = "Personal Attack Detection")
        private PersonalAttackDetectionResponse personalAttackDetection;

        @ApiModelProperty(value = "Semantic Analysis")
        private SemanticAnalysisResponse semanticAnalysis;
    }
}
