package com.datascouting.dachs.detection.service.config.properties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Set;

@Configuration
@ConfigurationProperties("dachs-detection-api")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DetectionApiConfigurationProperties {

    private List<String> administrationAllowedIps;

    private String hateSpeechDetectionHost;
    private String languageDetectionHost;
    private String personalAttackDetectionHost;
    private String semanticAnalysisHost;

    private Set<String> availableLanguages;

    private JwtConfig jwt;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class JwtConfig {
        private Long tokenExpire;
        private String tokenSecret;
    }
}
