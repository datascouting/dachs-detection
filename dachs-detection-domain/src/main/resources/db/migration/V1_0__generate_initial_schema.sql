create table users
(
  id       varchar(255) not null
    constraint users_id_pkey
    primary key,
  email    varchar(255) not null,
  enabled  boolean default false,
  name     varchar(255) not null,
  password varchar(255) not null,
  username varchar(255) not null
);

create table user_roles
(
  user_id varchar(255) not null
    constraint user_roles_user_id_fkey
    references users,
  roles   varchar(255)
);
