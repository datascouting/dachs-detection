package com.datascouting.dachs.detection.domain.repository;

import org.intellift.sol.domain.Identifiable;
import org.intellift.sol.domain.querydsl.jpa.repository.QueryDslJpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface GenericRepository<E extends Identifiable<String>> extends QueryDslJpaRepository<E, String> {
}
