package com.datascouting.dachs.detection.domain.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.intellift.sol.domain.Identifiable;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public abstract class AbstractEntity implements Identifiable<String> {

    @Id
    @GeneratedValue(generator = "java-uuid")
    @GenericGenerator(name = "java-uuid", strategy = "com.datascouting.dachs.detection.domain.entity.generator.UUIDGenerator")
    protected String id;
}
