package com.datascouting.dachs.detection.domain.repository;

import com.datascouting.dachs.detection.domain.entity.User;
import javaslang.control.Option;
import org.intellift.sol.domain.querydsl.jpa.repository.QueryDslJpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends QueryDslJpaRepository<User, String> {

    Option<User> findOneByUsername(String username);
}
