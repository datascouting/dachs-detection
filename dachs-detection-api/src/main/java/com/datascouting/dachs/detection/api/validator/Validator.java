package com.datascouting.dachs.detection.api.validator;

import javaslang.control.Try;

import javax.annotation.Nullable;

public interface Validator<T> {

    Try<T> validate(@Nullable T target);
}
