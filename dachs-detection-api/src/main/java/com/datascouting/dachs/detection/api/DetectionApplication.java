package com.datascouting.dachs.detection.api;

import com.datascouting.dachs.detection.domain.DachsDetectionDomainConfiguration;
import com.datascouting.dachs.detection.mapper.DachsDetectionMapperConfiguration;
import com.datascouting.dachs.detection.service.crud.DachsDetectionServiceCrudConfiguration;
import com.datascouting.dachs.detection.service.detection.DachsDetectionServiceDetectionConfiguration;
import com.datascouting.dachs.detection.service.security.DachsDetectionServiceSecurityConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.intellift.sol.controller.SolControllerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@Slf4j
@Import({
        SolControllerConfiguration.class,
        DachsDetectionDomainConfiguration.class,
        DachsDetectionMapperConfiguration.class,
        DachsDetectionServiceCrudConfiguration.class,
        DachsDetectionServiceDetectionConfiguration.class,
        DachsDetectionServiceSecurityConfiguration.class,
})
@SpringBootApplication
public class DetectionApplication {

    public static void main(final String... args) {
        SpringApplication.run(DetectionApplication.class, args);
    }
}
