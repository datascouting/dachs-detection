package com.datascouting.dachs.detection.api.controller.auth;

import com.datascouting.dachs.detection.common.constant.DetectionApiEndpoints;
import com.datascouting.dachs.detection.dto.request.TokenRequest;
import com.datascouting.dachs.detection.dto.response.TokenResponse;
import com.datascouting.dachs.detection.mapper.UserMapper;
import com.datascouting.dachs.detection.service.security.service.JwtTokenProvider;
import com.datascouting.dachs.detection.service.security.service.SecurityUserDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;
import java.util.function.Function;

@Api(
        tags = "Authentication",
        description = "Provides authentication mechanism. Dachs Detection is using JWT tokens in order to authorize requests."
)
@Slf4j
@RequiredArgsConstructor
@RestController
public class AuthControllerImpl implements AuthController {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final SecurityUserDetailsService securityUserDetailsService;
    private final UserMapper userMapper;

    @Override
    @ApiOperation(
            value = "Get authenticated using your credentials",
            response = TokenRequest.class
    )
    @PostMapping(DetectionApiEndpoints.AUTH_TOKEN)
    public ResponseEntity<TokenResponse> publishToken(@RequestBody final TokenRequest request) {
        Objects.requireNonNull(request, "request is null");

        final String username = request.getUsername();
        final String password = request.getPassword();

        return Try.of(() -> authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password)))
                .flatMap(authentication -> Try.of(() -> securityUserDetailsService.loadUserByUsername(username)))
                .flatMap(SecurityUserDetailsService::securityUserToUser)
                .flatMap(user -> Try.of(() -> userMapper.mapTo(user)))
                .flatMap(userDto -> jwtTokenProvider.createToken(username, userDto.getRoles())
                        .flatMap(token -> Try.of(() -> TokenResponse.builder()
                                .token(token)
                                .user(userDto)
                                .build())))
                .map(ResponseEntity::ok)
                .getOrElseThrow((Function<Throwable, RuntimeException>) RuntimeException::new);
    }
}
