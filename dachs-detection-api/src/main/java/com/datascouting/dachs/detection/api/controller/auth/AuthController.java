package com.datascouting.dachs.detection.api.controller.auth;

import com.datascouting.dachs.detection.dto.request.TokenRequest;
import com.datascouting.dachs.detection.dto.response.TokenResponse;
import org.springframework.http.ResponseEntity;

public interface AuthController {

    ResponseEntity<TokenResponse> publishToken(TokenRequest request);
}
