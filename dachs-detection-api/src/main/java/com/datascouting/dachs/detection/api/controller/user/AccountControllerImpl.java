package com.datascouting.dachs.detection.api.controller.user;

import com.datascouting.dachs.detection.common.constant.DetectionApiEndpoints;
import com.datascouting.dachs.detection.domain.entity.User;
import com.datascouting.dachs.detection.dto.UserDto;
import com.datascouting.dachs.detection.mapper.UserMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Objects;
import java.util.function.Function;

@Api(
        tags = "Accounts",
        description = "By using \"Accounts\" resources you can access your Account Information."
)
@Slf4j
@RequiredArgsConstructor
@RestController
public class AccountControllerImpl implements AccountController {

    private final UserMapper userMapper;

    @Override
    @ApiOperation(
            value = "Get the current user information",
            response = UserDto.class
    )
    @GetMapping(DetectionApiEndpoints.ACCOUNTS_ME)
    public ResponseEntity<UserDto> me(@ApiIgnore @AuthenticationPrincipal final User user) {
        Objects.requireNonNull(user, "user is null");

        return Try.of(() -> userMapper.mapTo(user))
                .map(ResponseEntity::ok)
                .getOrElseThrow((Function<Throwable, RuntimeException>) RuntimeException::new);
    }
}
