package com.datascouting.dachs.detection.api.filter;

import com.datascouting.dachs.detection.service.security.service.JwtTokenProvider;
import io.jsonwebtoken.JwtException;
import javaslang.control.Option;
import javaslang.control.Try;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Objects;

public class JwtTokenFilter extends GenericFilterBean {

    private final JwtTokenProvider jwtTokenProvider;

    public JwtTokenFilter(final JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    private static Option<String> resolveToken(final HttpServletRequest req) {
        Objects.requireNonNull(req, "req is null");

        return Option.of(req.getHeader("Authorization"))
                .flatMap(bearerToken -> {
                    if (bearerToken.length() > 7) {
                        final String bearer = bearerToken.substring(0, 7)
                                .trim()
                                .toLowerCase();

                        if (bearer.equals("bearer")) {
                            return Option.of(bearerToken.substring(7));
                        }
                    }

                    return Option.none();
                });
    }

    @Override
    public void doFilter(final ServletRequest req,
                         final ServletResponse res,
                         final FilterChain filterChain)
            throws IOException, ServletException {
        JwtTokenFilter.resolveToken((HttpServletRequest) req)
                .forEach(token -> {
                    final Boolean validToken = jwtTokenProvider.validateToken(token)
                            .getOrElseThrow(throwable -> new JwtException("Could not authenticate request", throwable));

                    if (validToken) {
                        final Authentication auth = Option.of(token)
                                .map(jwtTokenProvider::getAuthentication)
                                .getOrElse(() -> null);

                        SecurityContextHolder.getContext().setAuthentication(auth);
                    }
                });

        filterChain.doFilter(req, res);
    }
}
