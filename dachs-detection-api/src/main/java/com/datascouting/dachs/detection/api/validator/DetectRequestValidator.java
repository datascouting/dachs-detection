package com.datascouting.dachs.detection.api.validator;

import com.datascouting.dachs.detection.dto.request.DetectRequest;

public interface DetectRequestValidator extends Validator<DetectRequest> {
}
