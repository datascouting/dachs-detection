package com.datascouting.dachs.detection.api.controller.advice;

import com.datascouting.dachs.detection.api.validator.exception.ValidationException;
import io.jsonwebtoken.JwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler {

    private static String logWarnLevelExceptionMessage(Exception e) {
        log.warn("Caught exception while handling a request: " + getExceptionMessage(e));
        return e.getClass().getSimpleName();
    }

    private static String getExceptionMessage(Exception e) {
        return StringUtils.hasText(e.getMessage()) ? e.getMessage() : e.getClass().getSimpleName();
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<VndErrors> handleValidationException(final ValidationException validationException) {
        final List<VndErrors.VndError> errors = new ArrayList<>();

        validationException.getErrors()
                .forEach((fieldName, errorMessages) ->
                        errorMessages.forEach(errorMessage -> errors.add(
                                new VndErrors.VndError(
                                        logWarnLevelExceptionMessage(validationException),
                                        errorMessage
                                ))
                        ));

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new VndErrors(errors));
    }

    @ExceptionHandler(LockedException.class)
    public ResponseEntity<VndErrors> handleLockedException(final LockedException exception) {
        final List<VndErrors.VndError> errors = new ArrayList<>();

        errors.add(new VndErrors.VndError(
                logWarnLevelExceptionMessage(exception),
                getExceptionMessage(exception)
        ));

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(new VndErrors(errors));
    }

    @ExceptionHandler(DisabledException.class)
    public ResponseEntity<VndErrors> handleDisabledException(final DisabledException exception) {
        final List<VndErrors.VndError> errors = new ArrayList<>();

        errors.add(new VndErrors.VndError(
                logWarnLevelExceptionMessage(exception),
                getExceptionMessage(exception)
        ));

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(new VndErrors(errors));
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<VndErrors> handleBadCredentialsException(final BadCredentialsException exception) {
        final List<VndErrors.VndError> errors = new ArrayList<>();

        errors.add(new VndErrors.VndError(
                logWarnLevelExceptionMessage(exception),
                getExceptionMessage(exception)
        ));

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(new VndErrors(errors));
    }

    @ExceptionHandler(JwtException.class)
    public ResponseEntity<VndErrors> handleJwtExceptionException(final JwtException exception) {
        final List<VndErrors.VndError> errors = new ArrayList<>();

        errors.add(new VndErrors.VndError(
                logWarnLevelExceptionMessage(exception),
                getExceptionMessage(exception)
        ));

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(new VndErrors(errors));
    }
}
