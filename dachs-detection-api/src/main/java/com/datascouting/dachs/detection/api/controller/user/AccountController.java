package com.datascouting.dachs.detection.api.controller.user;

import com.datascouting.dachs.detection.domain.entity.User;
import com.datascouting.dachs.detection.dto.UserDto;
import org.springframework.http.ResponseEntity;

public interface AccountController {

    ResponseEntity<UserDto> me(User user);
}
