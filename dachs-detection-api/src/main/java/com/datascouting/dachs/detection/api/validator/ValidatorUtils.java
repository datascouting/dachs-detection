package com.datascouting.dachs.detection.api.validator;

import javaslang.control.Option;

import java.util.Objects;

public class ValidatorUtils {

    public static <T> boolean isNull(T input) {
        return Objects.isNull(input);
    }

    public static boolean isEmpty(String input) {
        return Option.of(input)
                .getOrElse("")
                .trim()
                .length() == 0;
    }
}
