package com.datascouting.dachs.detection.api.configuration.security;

import com.datascouting.dachs.detection.service.security.service.JwtTokenProvider;
import com.datascouting.dachs.detection.service.security.service.SecurityUserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@EnableWebSecurity
@Configuration
@RequiredArgsConstructor
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final SecurityUserDetailsService securityUserDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public void configure(final AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(securityUserDetailsService)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(final HttpSecurity security) throws Exception {
        security
                .authorizeRequests()

                .mvcMatchers(publicPatterns())
                .permitAll()

                .anyRequest()
                .authenticated()

                .and()

                .csrf()
                .disable()

                .cors()

                .and()

                .exceptionHandling()
                .authenticationEntryPoint(new HttpStatusEntryPoint(UNAUTHORIZED))

                .and()

                .apply(new JwtConfigurer(jwtTokenProvider));
    }

    private String[] publicPatterns() {
        return new String[]{
                "/auth/token",
                "/favicon.ico",
                "/robots.txt",
                "/swagger-ui.html",
                "/swagger-resources/**",
                "/v2/api-docs",
                "/webjars/**",

                "/detection"
        };
    }
}
