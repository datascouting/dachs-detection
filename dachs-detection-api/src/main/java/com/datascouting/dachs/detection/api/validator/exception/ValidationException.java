package com.datascouting.dachs.detection.api.validator.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class ValidationException extends Exception {

    private String message;
    private Map<String, List<String>> errors = new HashMap<>();

    public ValidationException(String message) {
        this.message = message;
    }

    public ValidationException(String message, Map<String, List<String>> errors) {
        this.message = message;
        this.errors = errors;
    }
}
