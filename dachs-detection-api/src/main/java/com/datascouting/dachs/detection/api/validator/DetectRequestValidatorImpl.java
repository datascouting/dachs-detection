package com.datascouting.dachs.detection.api.validator;

import com.datascouting.dachs.detection.api.validator.exception.ValidationException;
import com.datascouting.dachs.detection.dto.request.DetectRequest;
import com.datascouting.dachs.detection.service.detection.system.LanguageService;
import javaslang.control.Option;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.util.*;

@Slf4j
@Component
@RequiredArgsConstructor
public class DetectRequestValidatorImpl implements DetectRequestValidator {

    private static final String TEXT_FIELD_NAME = "text";
    private static final String LANGUAGES_FIELD_NAME = "languages";

    private final LanguageService languageService;

    @Override
    public Try<DetectRequest> validate(@Nullable final DetectRequest detectRequest) {
        return Option.of(detectRequest)
                .toTry(() -> new ValidationException("DetectRequest is empty"))
                .flatMap(detectReq -> {
                    final Map<String, List<String>> errors = new HashMap<>();

                    if (ValidatorUtils.isEmpty(detectReq.getText())) {
                        errors.put(TEXT_FIELD_NAME, Collections.singletonList("DetectRequest text is empty"));
                    }

                    Option.of(detectReq.getLanguages())
                            .forEach(languages ->
                                    languages.forEach(language ->
                                            languageService.isAvailable(language)
                                                    .filter(availableLanguageCode -> !availableLanguageCode._1)
                                                    .forEach(availableLanguageCode -> {
                                                        errors.computeIfAbsent(LANGUAGES_FIELD_NAME, k -> new ArrayList<>());

                                                        final List<String> languageErrors = errors.get(LANGUAGES_FIELD_NAME);
                                                        languageErrors.add(availableLanguageCode._2 + " is not a valid language");

                                                        errors.put(LANGUAGES_FIELD_NAME, languageErrors);
                                                    })));
                    if (errors.size() == 0) {
                        return Try.success(detectReq);
                    } else {
                        errors.forEach((field, error) ->
                                log.error(String.format("Field: %s, Errors: %s", field, String.join(",", error))));
                        return Try.failure(new ValidationException("Field validation failed", errors));
                    }
                });
    }
}
