package com.datascouting.dachs.detection.api.controller.detection;

import com.datascouting.dachs.detection.dto.DetectionDto;
import com.datascouting.dachs.detection.dto.request.DetectRequest;
import org.springframework.http.ResponseEntity;

public interface DetectionController {

    ResponseEntity<DetectionDto> detect(DetectRequest detectRequest) throws Throwable;
}
