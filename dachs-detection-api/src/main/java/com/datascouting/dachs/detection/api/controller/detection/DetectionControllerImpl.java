package com.datascouting.dachs.detection.api.controller.detection;

import com.datascouting.dachs.detection.api.validator.DetectRequestValidator;
import com.datascouting.dachs.detection.api.validator.exception.ValidationException;
import com.datascouting.dachs.detection.common.constant.DetectionApiEndpoints;
import com.datascouting.dachs.detection.dto.DetectionDto;
import com.datascouting.dachs.detection.dto.request.DetectRequest;
import com.datascouting.dachs.detection.service.detection.system.DetectionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.function.Function;

import static java.util.Objects.requireNonNull;

@Api(
        tags = "Detection",
        description = "Detection services."
)
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(DetectionApiEndpoints.RPC_DETECTION)
public class DetectionControllerImpl implements DetectionController {

    private final DetectionService detectionService;
    private final DetectRequestValidator detectRequestValidator;

    @Override
    @ApiOperation(
            value = "Perform detection for requested services and requested languages for provided text",
            response = DetectionDto.class
    )
    @PostMapping
    public ResponseEntity<DetectionDto> detect(@RequestBody final DetectRequest detectRequest) throws Throwable {
        requireNonNull(detectRequest, "detectRequest is null");

        return detectRequestValidator.validate(detectRequest)
                .flatMap(detectionService::detect)
                .map(ResponseEntity::ok)
                .onFailure(throwable -> {
                    if (throwable instanceof ValidationException) {
                        log.error("Error occurred while executing detection");
                    } else {
                        log.error("Error occurred while executing detection", throwable);
                    }
                })
                .getOrElseThrow(Function.identity());
    }
}
