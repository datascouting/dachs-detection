package com.datascouting.dachs.detection.messaging.handler;

import com.datascouting.dachs.detection.common.constant.DetectChannels;
import com.datascouting.dachs.detection.dto.request.DetectRequest;
import com.datascouting.dachs.detection.dto.response.DetectResponse;
import com.datascouting.dachs.detection.service.detection.system.DetectionService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class DetectionHandlerImpl implements DetectionHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final DetectionService detectionService;

    @Override
    @StreamListener(DetectChannels.DETECT_REQUESTS)
    @SendTo(DetectChannels.DETECT_RESULTS)
    public Message<DetectResponse> detect(@Nonnull final DetectRequest detectRequest) {
        Objects.requireNonNull(detectRequest, "detectRequest is null");

        logger.info("Received detection request with correlation id " + detectRequest.getCorrelationId());

        return detectionService.detect(detectRequest)
                .map(detectionDto -> new DetectResponse(detectRequest.getCorrelationId(), detectionDto))
                .map(detectResponse -> MessageBuilder
                        .withPayload(detectResponse)
                        .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON_VALUE)
                        .build())
                .getOrElseThrow((Function<Throwable, RuntimeException>) RuntimeException::new);
    }
}
