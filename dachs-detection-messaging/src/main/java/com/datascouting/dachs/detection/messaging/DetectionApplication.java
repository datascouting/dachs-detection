package com.datascouting.dachs.detection.messaging;

import com.datascouting.dachs.detection.common.constant.DetectChannels;
import com.datascouting.dachs.detection.domain.DachsDetectionDomainConfiguration;
import com.datascouting.dachs.detection.mapper.DachsDetectionMapperConfiguration;
import com.datascouting.dachs.detection.service.crud.DachsDetectionServiceCrudConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Import;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

@Slf4j
@Import({
        DachsDetectionDomainConfiguration.class,
        DachsDetectionMapperConfiguration.class,
        DachsDetectionServiceCrudConfiguration.class,
})
@EnableBinding({
        DetectionApplication.DetectBindings.class,
})
@SpringBootApplication
public class DetectionApplication {

    public static void main(final String... args) {
        SpringApplication.run(DetectionApplication.class, args);
    }

    @Autowired
    public void customFactoryConfigurer(final CachingConnectionFactory cachingConnectionFactory) {
        cachingConnectionFactory.getRabbitConnectionFactory().setRequestedFrameMax(0);
    }

    public interface DetectBindings {

        @Input(DetectChannels.DETECT_REQUESTS)
        SubscribableChannel detectRequests();

        @Output(DetectChannels.DETECT_RESULTS)
        MessageChannel detectResults();
    }
}
