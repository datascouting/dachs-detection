package com.datascouting.dachs.detection.messaging.handler;

import com.datascouting.dachs.detection.dto.request.DetectRequest;
import com.datascouting.dachs.detection.dto.response.DetectResponse;
import org.springframework.messaging.Message;

public interface DetectionHandler {

    Message<DetectResponse> detect(DetectRequest detectRequest);
}
