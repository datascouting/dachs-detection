package com.datascouting.dachs.detection.service.detection.microservice.personal.attack.detection;

import com.datascouting.dachs.detection.dto.microservice.personal.attack.detection.PersonalAttackDetectionRequest;
import com.datascouting.dachs.detection.dto.microservice.personal.attack.detection.PersonalAttackDetectionResponse;
import javaslang.control.Try;

public interface PersonalAttackDetectionClient {

    Try<PersonalAttackDetectionResponse> detect(PersonalAttackDetectionRequest personalAttackDetectionRequest);
}
