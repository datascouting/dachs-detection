package com.datascouting.dachs.detection.service.detection;

import com.datascouting.dachs.detection.service.config.DachsDetectionServiceConfigConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan
@Import(DachsDetectionServiceConfigConfiguration.class)
public class DachsDetectionServiceDetectionConfiguration {
}
