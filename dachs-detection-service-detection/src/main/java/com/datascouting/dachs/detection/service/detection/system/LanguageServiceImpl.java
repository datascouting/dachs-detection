package com.datascouting.dachs.detection.service.detection.system;

import com.datascouting.dachs.detection.service.config.properties.DetectionApiConfigurationProperties;
import javaslang.Tuple;
import javaslang.Tuple2;
import javaslang.control.Try;
import org.springframework.stereotype.Service;

import java.util.Set;

import static com.datascouting.dachs.detection.common.constant.Languages.UNDEFINED;
import static java.util.Objects.requireNonNull;

@Service
public class LanguageServiceImpl implements LanguageService {

    private final Set<String> availableLanguages;

    public LanguageServiceImpl(DetectionApiConfigurationProperties properties) {
        availableLanguages = properties.getAvailableLanguages();
        availableLanguages.add(UNDEFINED);
    }

    @Override
    public Try<Tuple2<Boolean, String>> isAvailable(final String language) {
        requireNonNull(language, "language is null");

        return Try.of(() -> Tuple.of(
                availableLanguages.contains(language),
                language
        ));
    }
}
