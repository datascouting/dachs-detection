package com.datascouting.dachs.detection.service.detection.system;

import javaslang.Tuple2;
import javaslang.control.Try;

public interface LanguageService {

    Try<Tuple2<Boolean, String>> isAvailable(String language);
}
