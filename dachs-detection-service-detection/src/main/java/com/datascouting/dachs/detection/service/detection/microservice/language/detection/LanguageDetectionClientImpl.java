package com.datascouting.dachs.detection.service.detection.microservice.language.detection;

import com.datascouting.dachs.detection.dto.microservice.language.detection.LanguageDetectionRequest;
import com.datascouting.dachs.detection.dto.microservice.language.detection.LanguageDetectionResponse;
import com.datascouting.dachs.detection.service.config.properties.DetectionApiConfigurationProperties;
import javaslang.control.Option;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static java.util.Objects.requireNonNull;

@Service
@RequiredArgsConstructor
@Slf4j
public class LanguageDetectionClientImpl implements LanguageDetectionClient {

    private final RestTemplate restTemplate;
    private final DetectionApiConfigurationProperties properties;

    @Override
    public Try<LanguageDetectionResponse> detect(final LanguageDetectionRequest languageDetectionRequest) {
        requireNonNull(languageDetectionRequest);

        return detectUrl()
                .onFailure(throwable -> log.error("Could not construct Language detection url", throwable))
                .flatMap(detectionUrl -> Try.of(() -> restTemplate.exchange(
                        detectionUrl,
                        HttpMethod.POST,
                        new HttpEntity<>(languageDetectionRequest),
                        LanguageDetectionResponse.class))
                        .onFailure(throwable -> {
                            log.error(String.format("Could not execute POST request to endpoint: %s", detectionUrl), throwable);
                            if (throwable instanceof HttpClientErrorException) {
                                log.error(String.format("Response Body: %s", ((HttpClientErrorException) throwable).getResponseBodyAsString()), throwable);
                            }
                        })
                )
                .map(HttpEntity::getBody);
    }

    private Try<String> detectUrl() {
        return Option.of(properties)
                .flatMap(properties -> Option.of(properties.getLanguageDetectionHost()))
                .map(host -> String.join("/", host, "detection"))
                .toTry();
    }
}
