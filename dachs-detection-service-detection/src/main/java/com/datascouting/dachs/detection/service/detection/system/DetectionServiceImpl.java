package com.datascouting.dachs.detection.service.detection.system;

import com.datascouting.dachs.detection.dto.DetectionDto;
import com.datascouting.dachs.detection.dto.microservice.hate.speech.detection.HateSpeechDetectionRequest;
import com.datascouting.dachs.detection.dto.microservice.hate.speech.detection.HateSpeechDetectionResponse;
import com.datascouting.dachs.detection.dto.microservice.language.detection.LanguageDetectionRequest;
import com.datascouting.dachs.detection.dto.microservice.personal.attack.detection.PersonalAttackDetectionRequest;
import com.datascouting.dachs.detection.dto.microservice.personal.attack.detection.PersonalAttackDetectionResponse;
import com.datascouting.dachs.detection.dto.microservice.semantic.analysis.SemanticAnalysisRequest;
import com.datascouting.dachs.detection.dto.microservice.semantic.analysis.SemanticAnalysisResponse;
import com.datascouting.dachs.detection.dto.request.DetectRequest;
import com.datascouting.dachs.detection.service.detection.microservice.hate.speech.detection.HateSpeechDetectionClient;
import com.datascouting.dachs.detection.service.detection.microservice.language.detection.LanguageDetectionClient;
import com.datascouting.dachs.detection.service.detection.microservice.personal.attack.detection.PersonalAttackDetectionClient;
import com.datascouting.dachs.detection.service.detection.microservice.semantic.analysis.SemanticAnalysisClient;
import javaslang.Value;
import javaslang.control.Option;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.datascouting.dachs.detection.common.constant.Languages.UNDEFINED;
import static java.util.Objects.requireNonNull;

@Service
@RequiredArgsConstructor
public class DetectionServiceImpl implements DetectionService {

    private final LanguageDetectionClient languageDetectionClient;
    private final LanguageService languageService;

    private final HateSpeechDetectionClient hateSpeechDetectionClient;
    private final PersonalAttackDetectionClient personalAttackDetectionClient;
    private final SemanticAnalysisClient semanticAnalysisClient;

    @Override
    public Try<DetectionDto> detect(final DetectRequest detectRequest) {
        requireNonNull(detectRequest, "detectRequest is null");

        return useHateSpeechDetection(detectRequest)
                .flatMap(useHateSpeechDetection -> usePersonalAttachDetection(detectRequest)
                        .flatMap(usePersonalAttachDetection -> useSemanticAnalysis(detectRequest)
                                .flatMap(useSemanticAnalysis -> getDetectionLanguages(detectRequest)
                                        .flatMap(languages -> Try.sequence(languages.stream()
                                                .map(language -> createLanguageDetection(
                                                        language,
                                                        detectRequest,
                                                        useHateSpeechDetection,
                                                        usePersonalAttachDetection,
                                                        useSemanticAnalysis
                                                ))
                                                .collect(Collectors.toSet()))
                                                .map(Value::toJavaSet)
                                                .map(DetectionDto::new)
                                        ))));
    }

    private Try<DetectionDto.LanguageDetection> createLanguageDetection(final String language,
                                                                        final DetectRequest detectRequest,
                                                                        final Boolean useHateSpeechDetection,
                                                                        final Boolean usePersonalAttachDetection,
                                                                        final Boolean useSemanticAnalysis) {
        requireNonNull(language, "language is null");
        requireNonNull(detectRequest, "detectRequest is null");
        requireNonNull(useHateSpeechDetection, "useHateSpeechDetection is null");
        requireNonNull(usePersonalAttachDetection, "usePersonalAttachDetection is null");
        requireNonNull(useSemanticAnalysis, "useSemanticAnalysis is null");

        final Try<HateSpeechDetectionResponse> hateSpeechDetectionResponses = useHateSpeechDetection
                ? hateSpeechDetectionClient.detect(
                HateSpeechDetectionRequest.builder()
                        .language(language)
                        .text(detectRequest.getText())
                        .build())
                : Try.success(null);

        final Try<PersonalAttackDetectionResponse> personalAttackDetectionResponses = usePersonalAttachDetection
                ? personalAttackDetectionClient.detect(
                PersonalAttackDetectionRequest.builder()
                        .language(language)
                        .text(detectRequest.getText())
                        .build())
                : Try.success(null);

        final Try<SemanticAnalysisResponse> semanticAnalysisResponses = useSemanticAnalysis
                ? semanticAnalysisClient.analyze(new SemanticAnalysisRequest(language, detectRequest.getText()))
                : Try.success(null);

        return hateSpeechDetectionResponses.flatMap(hateSpeechDetectionResponse ->
                personalAttackDetectionResponses.flatMap(personalAttackDetectionResponse ->
                        semanticAnalysisResponses.flatMap(semanticAnalysisResponse -> Try.of(() -> {
                            final DetectionDto.LanguageDetection languageDetection = new DetectionDto.LanguageDetection();
                            languageDetection.setLanguage(language);
                            languageDetection.setHateSpeechDetection(hateSpeechDetectionResponse);
                            languageDetection.setPersonalAttackDetection(personalAttackDetectionResponse);
                            languageDetection.setSemanticAnalysis(semanticAnalysisResponse);

                            return languageDetection;
                        }))));
    }

    private Try<Set<String>> getDetectionLanguages(final DetectRequest detectRequest) {
        requireNonNull(detectRequest, "detectRequest is null");

        final Function<Set<String>, Set<String>> getAvailableLanguages = languages -> languages.stream()
                .map(languageService::isAvailable)
                .filter(Try::isSuccess)
                .map(Try::get)
                .filter(availableLanguageCode -> !availableLanguageCode._2.equals(UNDEFINED))
                .filter(availableLanguageCode -> availableLanguageCode._1)
                .map(availableLanguageCode -> availableLanguageCode._2)
                .collect(Collectors.toSet());

        final Set<String> requestedLanguages = detectRequest.getLanguages();
        if (requestedLanguages != null && !requestedLanguages.isEmpty()) {
            return Try.of(() -> getAvailableLanguages.apply(requestedLanguages));
        } else {
            return languageDetectionClient.detect(new LanguageDetectionRequest(detectRequest.getText()))
                    .map(HashMap::keySet)
                    .map(getAvailableLanguages);
        }
    }

    private Try<Boolean> useHateSpeechDetection(final DetectRequest detectRequest) {
        requireNonNull(detectRequest, "detectRequest is null");

        return canUseAllServices(detectRequest)
                .map(useAllServices -> useAllServices
                        ? true
                        : Option.of(detectRequest.getHateSpeechDetection())
                        .getOrElse(false));
    }

    private Try<Boolean> usePersonalAttachDetection(final DetectRequest detectRequest) {
        requireNonNull(detectRequest, "detectRequest is null");

        return canUseAllServices(detectRequest)
                .map(useAllServices -> useAllServices
                        ? true
                        : Option.of(detectRequest.getPersonalAttackDetection())
                        .getOrElse(false));
    }

    private Try<Boolean> useSemanticAnalysis(final DetectRequest detectRequest) {
        requireNonNull(detectRequest, "detectRequest is null");

        return canUseAllServices(detectRequest)
                .map(useAllServices -> useAllServices
                        ? true
                        : Option.of(detectRequest.getSemanticAnalysis())
                        .getOrElse(false));
    }

    private Try<Boolean> canUseAllServices(final DetectRequest detectRequest) {
        requireNonNull(detectRequest, "detectRequest is null");

        return Try.of(() -> {
            final Boolean hateSpeechDetection = detectRequest.getHateSpeechDetection();
            final Boolean personalAttackDetection = detectRequest.getPersonalAttackDetection();
            final Boolean semanticAnalysis = detectRequest.getSemanticAnalysis();

            return hateSpeechDetection == null
                    && personalAttackDetection == null
                    && semanticAnalysis == null;
        });
    }
}
