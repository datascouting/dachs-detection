package com.datascouting.dachs.detection.service.detection.microservice.language.detection;

import com.datascouting.dachs.detection.dto.microservice.language.detection.LanguageDetectionRequest;
import com.datascouting.dachs.detection.dto.microservice.language.detection.LanguageDetectionResponse;
import javaslang.control.Try;

public interface LanguageDetectionClient {

    Try<LanguageDetectionResponse> detect(LanguageDetectionRequest languageDetectionRequest);
}
