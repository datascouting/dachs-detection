package com.datascouting.dachs.detection.service.detection.microservice.semantic.analysis;

import com.datascouting.dachs.detection.dto.microservice.semantic.analysis.SemanticAnalysisRequest;
import com.datascouting.dachs.detection.dto.microservice.semantic.analysis.SemanticAnalysisResponse;
import com.datascouting.dachs.detection.service.config.properties.DetectionApiConfigurationProperties;
import javaslang.control.Option;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static java.util.Objects.requireNonNull;

@Service
@RequiredArgsConstructor
@Slf4j
public class SemanticAnalysisClientImpl implements SemanticAnalysisClient {

    private final RestTemplate restTemplate;
    private final DetectionApiConfigurationProperties properties;

    @Override
    public Try<SemanticAnalysisResponse> analyze(final SemanticAnalysisRequest semanticAnalysisRequest) {
        requireNonNull(semanticAnalysisRequest);

        return analyzeUrl()
                .onFailure(throwable -> log.error("Could not construct Semantic analysis url", throwable))
                .flatMap(analyzeUrl -> Try.of(() -> restTemplate.exchange(
                        analyzeUrl,
                        HttpMethod.POST,
                        new HttpEntity<>(semanticAnalysisRequest),
                        SemanticAnalysisResponse.class))
                        .onFailure(throwable -> {
                            log.error(String.format("Could not execute POST request to endpoint: %s", analyzeUrl), throwable);
                            if (throwable instanceof HttpClientErrorException) {
                                log.error(String.format("Response Body: %s", ((HttpClientErrorException) throwable).getResponseBodyAsString()), throwable);
                            }
                        })
                )
                .map(HttpEntity::getBody);
    }

    private Try<String> analyzeUrl() {
        return Option.of(properties)
                .flatMap(properties -> Option.of(properties.getSemanticAnalysisHost()))
                .map(host -> String.join("/", host, "analysis"))
                .toTry();
    }
}
