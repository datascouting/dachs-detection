package com.datascouting.dachs.detection.service.detection.system;

import com.datascouting.dachs.detection.dto.DetectionDto;
import com.datascouting.dachs.detection.dto.request.DetectRequest;
import javaslang.control.Try;

public interface DetectionService {

    Try<DetectionDto> detect(DetectRequest detectRequest);
}
