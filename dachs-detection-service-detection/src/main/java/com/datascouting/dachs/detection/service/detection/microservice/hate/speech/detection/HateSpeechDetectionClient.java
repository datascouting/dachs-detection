package com.datascouting.dachs.detection.service.detection.microservice.hate.speech.detection;

import com.datascouting.dachs.detection.dto.microservice.hate.speech.detection.HateSpeechDetectionRequest;
import com.datascouting.dachs.detection.dto.microservice.hate.speech.detection.HateSpeechDetectionResponse;
import javaslang.control.Try;

public interface HateSpeechDetectionClient {

    Try<HateSpeechDetectionResponse> detect(HateSpeechDetectionRequest hateSpeechDetectionRequest);
}
