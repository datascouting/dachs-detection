package com.datascouting.dachs.detection.service.detection.microservice.semantic.analysis;

import com.datascouting.dachs.detection.dto.microservice.semantic.analysis.SemanticAnalysisRequest;
import com.datascouting.dachs.detection.dto.microservice.semantic.analysis.SemanticAnalysisResponse;
import javaslang.control.Try;

public interface SemanticAnalysisClient {

    Try<SemanticAnalysisResponse> analyze(SemanticAnalysisRequest semanticAnalysisRequest);
}
