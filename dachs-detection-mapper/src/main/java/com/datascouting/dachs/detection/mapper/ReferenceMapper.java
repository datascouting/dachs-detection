package com.datascouting.dachs.detection.mapper;

import com.datascouting.dachs.detection.domain.entity.AbstractEntity;
import com.datascouting.dachs.detection.dto.Reference;
import org.mapstruct.TargetType;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Objects;

@Component
public class ReferenceMapper {

    @PersistenceContext
    private EntityManager entityManager;

    public <T extends AbstractEntity> T resolve(final Reference reference, @TargetType final Class<T> entityClass) {
        if (Objects.nonNull(reference)) {
            return entityManager.find(entityClass, reference.getId());
        }

        return null;
    }

    public Reference toReference(final AbstractEntity entity) {
        if (Objects.nonNull(entity)) {
            final Reference reference = new Reference();

            reference.setId(entity.getId());

            return reference;
        }

        return null;
    }
}
