package com.datascouting.dachs.detection.mapper;

import com.datascouting.dachs.detection.domain.entity.User;
import com.datascouting.dachs.detection.dto.UserDto;
import org.intellift.sol.mapper.PageMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper extends PageMapper<User, UserDto> {
}
