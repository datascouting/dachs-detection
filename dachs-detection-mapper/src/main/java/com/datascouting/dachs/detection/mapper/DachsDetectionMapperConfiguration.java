package com.datascouting.dachs.detection.mapper;

import com.datascouting.dachs.detection.domain.DachsDetectionDomainConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan
@Import(DachsDetectionDomainConfiguration.class)
public class DachsDetectionMapperConfiguration {
}
