package com.datascouting.dachs.detection.common.constant;

public abstract class DetectionApiEndpoints {

    public static final String AUTH_TOKEN = "/auth/token";
    public static final String ACCOUNTS_ME = "/accounts/me";

    public static final String RPC_DETECTION = "/detection";
}
