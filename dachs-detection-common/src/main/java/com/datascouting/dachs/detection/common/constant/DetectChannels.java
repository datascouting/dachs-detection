package com.datascouting.dachs.detection.common.constant;

public abstract class DetectChannels {

    public static final String DETECT_REQUESTS = "detect-requests";

    public static final String DETECT_RESULTS = "detect-results";
}
