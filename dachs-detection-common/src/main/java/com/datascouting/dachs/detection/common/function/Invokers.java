package com.datascouting.dachs.detection.common.function;

import java.util.Objects;
import java.util.function.Supplier;

public abstract class Invokers {

    public static <T> T invoke(final Supplier<T> f) {
        Objects.requireNonNull(f, "f is null");

        return f.get();
    }

    public static void invoke(final Runnable f) {
        Objects.requireNonNull(f, "f is null");

        f.run();
    }
}
