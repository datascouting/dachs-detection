package com.datascouting.dachs.detection.common.constant;

public abstract class DachsDetectionConstants {

    public static final String DACHS_DETECTION_API_PACKAGE = "com.datascouting.dachs.detection.api";
}
