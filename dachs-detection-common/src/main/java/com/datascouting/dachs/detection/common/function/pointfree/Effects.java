package com.datascouting.dachs.detection.common.function.pointfree;


import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

public abstract class Effects {

    public static <U> Function<Object, U> replaceWith(final Supplier<? extends U> newValueSupplier) {
        Objects.requireNonNull(newValueSupplier, "newValueSupplier is null");

        return ignored -> newValueSupplier.get();
    }
}
