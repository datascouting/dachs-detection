package com.datascouting.dachs.detection.cli;

import com.datascouting.dachs.detection.service.crud.DachsDetectionServiceCrudConfiguration;
import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.shell.jline.PromptProvider;

@SpringBootApplication
@Import(DachsDetectionServiceCrudConfiguration.class)
public class DachsDetectionConsoleApplication {

    public static void main(String[] args) {
        SpringApplication.run(DachsDetectionConsoleApplication.class, args);
    }

    @Bean
    public PromptProvider promptProvider() {
        return () -> new AttributedString("dachs:>", AttributedStyle.DEFAULT.foreground(AttributedStyle.YELLOW));
    }
}