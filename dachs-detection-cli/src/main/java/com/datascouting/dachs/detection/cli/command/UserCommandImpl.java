package com.datascouting.dachs.detection.cli.command;

import com.datascouting.dachs.detection.cli.util.TextStyle;
import com.datascouting.dachs.detection.cli.util.ToString;
import com.datascouting.dachs.detection.domain.entity.AbstractEntity;
import com.datascouting.dachs.detection.domain.entity.QUser;
import com.datascouting.dachs.detection.domain.entity.User;
import com.datascouting.dachs.detection.service.crud.system.user.UserCrudService;
import com.querydsl.core.BooleanBuilder;
import javaslang.collection.List;
import javaslang.collection.Stream;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.jline.reader.LineReader;
import org.jline.terminal.Terminal;
import org.springframework.context.annotation.Lazy;
import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.ArrayList;

@Lazy
@ShellComponent
@ShellCommandGroup("User Commands")
@RequiredArgsConstructor
public class UserCommandImpl implements UserCommand {

    private final UserCrudService userCrudService;
    private final Terminal terminal;
    private final LineReader lineReader;

    @ShellMethod("Create a new user into dachs-detection")
    public String createUser(
            final String name,
            final String email,
            final String password,
            final Boolean enabled
    ) {
        final CommandOption selection = this.select(
                terminal,
                lineReader,
                String.format("Is this user data correct? \n" +
                                "Name: %s\n" +
                                "Email: %s\n" +
                                "Password: %s\n" +
                                "Enabled: %s\n",
                        name,
                        email,
                        password,
                        enabled
                ),
                List.of(
                        new CommandOption("Y", "yes"),
                        new CommandOption("N", "no")
                )
        );

        if ("Y".equals(selection.getId())) {
            final User user = new User();
            user.setName(name);
            user.setEmail(email);
            user.setUsername(email);
            user.setPassword(password);
            user.setEnabled(enabled);
            user.setRoles(new ArrayList<>());

            return userCrudService.create(user)
                    .map(AbstractEntity::getId)
                    .map(userId -> "User successfully created with id: " + userId)
                    .getOrElseGet(throwable -> "Could not create user. " + throwable.getMessage());
        } else {
            return "Operation canceled";
        }
    }

    @ShellMethod("Find users")
    public String findUsers() {
        final String name = input(lineReader, "Name (* for all): ");
        final String email = input(lineReader, "Email (* for all): ");
        final CommandOption selected = select(terminal, lineReader, "Enabled: ", List.of(
                new CommandOption("1", "Enabled"),
                new CommandOption("0", "Not Enabled"),
                new CommandOption("*", "All")
        ));

        Try<Stream<User>> foundUsers;

        if ("*".equals(name) && "*".equals(email) && "*".equals(selected.getId())) {
            foundUsers = userCrudService.findAll();
        } else {
            final QUser qUser = QUser.user;

            final BooleanBuilder builder = new BooleanBuilder();

            if (!"*".equals(name)) {
                builder.and(qUser.name.eq(name));
            }

            if (!"*".equals(email)) {
                builder.and(qUser.email.eq(name));
            }

            if (!"*".equals(selected.getId())) {
                builder.and(qUser.enabled.eq("1".equals(selected.getId())));
            }

            foundUsers = userCrudService.findAll(builder);
        }

        return foundUsers
                .map(users -> {
                    if (users.isEmpty()) {
                        return TextStyle.red("No users found");
                    } else {
                        return users
                                .map(ToString::user)
                                .foldLeft("", (state, user) -> state + "\n" + user);
                    }
                })
                .getOrElseGet(throwable -> TextStyle.red("Could perform find users."));
    }

    @ShellMethod("Enable user by id")
    public String enableUser(String userId) {
        return this.userCrudService.findOne(userId)
                .flatMap(users -> users.toTry(() -> new RuntimeException("Could not find user")))
                .flatMap(user -> {
                    user.setEnabled(true);
                    return userCrudService.update(user);
                })
                .map(user -> TextStyle.green("Successfully enabled"))
                .getOrElseGet(throwable -> TextStyle.red(throwable.getMessage()));
    }

    @ShellMethod("Disable user by id")
    public String disableUser(String userId) {
        return this.userCrudService.findOne(userId)
                .flatMap(users -> users.toTry(() -> new RuntimeException("Could not find user")))
                .flatMap(user -> {
                    user.setEnabled(false);
                    return userCrudService.update(user);
                })
                .map(user -> TextStyle.green("Successfully disabled"))
                .getOrElseGet(throwable -> TextStyle.red(throwable.getMessage()));
    }
}
