package com.datascouting.dachs.detection.cli.command;

import com.datascouting.dachs.detection.cli.util.TextStyle;
import javaslang.collection.List;
import javaslang.control.Option;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.jline.reader.LineReader;
import org.jline.terminal.Terminal;

import java.util.Objects;

public interface InteractiveCommand {

    default CommandOption select(
            final Terminal terminal,
            final LineReader lineReader,
            final String question,
            final List<CommandOption> options
    ) {
        Objects.requireNonNull(lineReader, "lineReader is null");
        Objects.requireNonNull(question, "question is null");

        CommandOption foundOption = null;
        boolean success = false;

        String optionText = "(Options: " +
                String.join("/", options
                        .map(CommandOption::getId)
                        .toJavaList())
                + ")";

        do {
            String input = lineReader.readLine("\n" + question + "\n" + optionText + "> ");

            Option<CommandOption> commandOptionOption = options.filter(commandOption -> commandOption.id.equals(input))
                    .headOption();

            if (commandOptionOption.isDefined()) {
                foundOption = commandOptionOption.get();
                success = true;
            } else {
                terminal.writer().println(
                        TextStyle.red("\nInput not valid. Please type an option and press Enter to select"));
            }
        } while (!success);

        return foundOption;
    }


    default String input(
            final LineReader lineReader,
            final String question
    ) {
        return lineReader.readLine("\n" + question + "> ");
    }

    @Data
    @RequiredArgsConstructor
    class CommandOption {
        private final String id;
        private final String option;
    }
}
