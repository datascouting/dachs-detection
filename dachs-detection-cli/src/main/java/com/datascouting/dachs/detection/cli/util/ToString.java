package com.datascouting.dachs.detection.cli.util;

import com.datascouting.dachs.detection.domain.entity.User;

import java.util.Objects;

public class ToString {

    public static String user(final User user) {
        Objects.requireNonNull(user);

        return "{\n" +
                "id: " + user.getId() + ",\n" +
                "name: " + user.getName() + ",\n" +
                "email: " + user.getEmail() + ",\n" +
                "enabled: " + user.getEnabled() + ",\n" +
                "}\n";
    }
}
