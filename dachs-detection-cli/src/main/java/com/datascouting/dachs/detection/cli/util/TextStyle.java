package com.datascouting.dachs.detection.cli.util;

public class TextStyle {

    private static String ANSI_RESET = "\u001B[0m";
    private static String ANSI_RED = "\u001B[31m";
    private static String ANSI_GREEN = "\u001B[32m";

    public static String red(final String message) {
        return ANSI_RED + message + ANSI_RESET;
    }

    public static String green(final String message) {
        return ANSI_GREEN + message + ANSI_RESET;
    }
}
