package com.datascouting.dachs.detection.sdk.client;

import com.datascouting.dachs.detection.dto.DetectionDto;
import com.datascouting.dachs.detection.dto.request.DetectRequest;
import com.datascouting.dachs.detection.sdk.config.DetectionSdkConfigurationProperties;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;

import static java.util.Objects.requireNonNull;

@Slf4j
@Service
@RequiredArgsConstructor
public class DetectionSdkClientImpl implements DetectionSdkClient {

    private final DetectionSdkConfigurationProperties properties;
    private final RestTemplate restTemplate;

    @Override
    public Try<DetectionDto> detect(final DetectRequest detectRequest) {
        requireNonNull(detectRequest, "detectRequest is null");

        return Try.of(() -> String.join("/", properties.getAddress(), "detection"))
                .flatMap(detectionUrl ->
                        Try.of(() -> {
                            final HttpHeaders httpHeaders = new HttpHeaders();
                            final String token = Base64.getEncoder().encodeToString((properties.getUser() + ":" + properties.getPassword()).getBytes());
                            httpHeaders.add("Authorization", "Basic " + token);
                            return httpHeaders;
                        })
                                .flatMap(httpHeaders -> Try.of(() -> restTemplate.exchange(
                                        detectionUrl,
                                        HttpMethod.POST,
                                        new HttpEntity<>(detectRequest, httpHeaders),
                                        DetectionDto.class
                                )))
                                .onFailure(throwable -> {
                                    log.error(String.format("Could not execute POST request to endpoint: %s, Error: %s", detectionUrl, throwable.getMessage()));
                                    if (throwable instanceof HttpClientErrorException) {
                                        log.error(String.format("Response Body: %s, Error: %s", ((HttpClientErrorException) throwable).getResponseBodyAsString(), throwable.getMessage()));
                                    }
                                })
                                .map(HttpEntity::getBody));
    }
}
