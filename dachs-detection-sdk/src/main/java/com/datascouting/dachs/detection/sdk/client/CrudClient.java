package com.datascouting.dachs.detection.sdk.client;

import com.datascouting.dachs.detection.dto.AbstractDto;
import org.intellift.sol.sdk.client.CrudApiClient;

public interface CrudClient<D extends AbstractDto> extends CrudApiClient<D, String> {
}
