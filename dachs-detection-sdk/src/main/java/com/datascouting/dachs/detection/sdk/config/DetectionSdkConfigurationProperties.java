package com.datascouting.dachs.detection.sdk.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "dachs-detection")
public class DetectionSdkConfigurationProperties {

    private String address;
    private String user;
    private String password;
}
