package com.datascouting.dachs.detection.sdk.client;

import com.datascouting.dachs.detection.dto.DetectionDto;
import com.datascouting.dachs.detection.dto.request.DetectRequest;
import javaslang.control.Try;

public interface DetectionSdkClient {

    Try<DetectionDto> detect(final DetectRequest detectRequest);
}
