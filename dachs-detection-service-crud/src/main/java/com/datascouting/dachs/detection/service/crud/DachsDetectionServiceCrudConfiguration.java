package com.datascouting.dachs.detection.service.crud;

import com.datascouting.dachs.detection.domain.DachsDetectionDomainConfiguration;
import com.datascouting.dachs.detection.service.config.DachsDetectionServiceConfigConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;

@Configuration
@ComponentScan
@Import({
        DachsDetectionDomainConfiguration.class,
        DachsDetectionServiceConfigConfiguration.class,
})
public class DachsDetectionServiceCrudConfiguration {

    @Bean
    @ConditionalOnMissingBean(RestTemplate.class)
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
