package com.datascouting.dachs.detection.service.crud.system;

import org.intellift.sol.domain.Identifiable;
import org.intellift.sol.service.querydsl.QueryDslCrudService;

public interface CrudService<E extends Identifiable<String>> extends QueryDslCrudService<E, String> {
}
