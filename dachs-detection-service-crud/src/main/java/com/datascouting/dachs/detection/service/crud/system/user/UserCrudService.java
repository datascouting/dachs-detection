package com.datascouting.dachs.detection.service.crud.system.user;

import com.datascouting.dachs.detection.domain.entity.User;
import com.datascouting.dachs.detection.service.crud.system.CrudService;
import javaslang.control.Option;
import javaslang.control.Try;

import java.util.function.Supplier;

public interface UserCrudService extends CrudService<User> {

    Try<Option<User>> findByUsername(String username);

    Try<User> findByUsername(String username, Supplier<? extends Exception> ifNotFound);
}
