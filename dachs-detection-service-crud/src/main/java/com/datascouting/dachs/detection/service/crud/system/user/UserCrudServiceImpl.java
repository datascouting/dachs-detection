package com.datascouting.dachs.detection.service.crud.system.user;

import com.datascouting.dachs.detection.domain.entity.User;
import com.datascouting.dachs.detection.domain.repository.UserRepository;
import javaslang.control.Option;
import javaslang.control.Try;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.function.Supplier;

@Service
public class UserCrudServiceImpl implements com.datascouting.dachs.detection.service.crud.system.user.UserCrudService {

    private final UserRepository userRepository;

    public UserCrudServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserRepository getRepository() {
        return userRepository;
    }

    @Override
    public Try<Option<User>> findByUsername(final String username) {
        Objects.requireNonNull(username, "username is null");

        return Try.of(() -> getRepository().findOneByUsername(username));
    }

    @Override
    public Try<User> findByUsername(final String username, final Supplier<? extends Exception> ifNotFound) {
        Objects.requireNonNull(username, "username is null");
        Objects.requireNonNull(ifNotFound, "ifNotFound is null");

        return findByUsername(username)
                .flatMap(userOption -> userOption.toTry(ifNotFound));
    }
}
