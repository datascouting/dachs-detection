package com.datascouting.dachs.detection.service.security.service;

import com.datascouting.dachs.detection.service.config.properties.DetectionApiConfigurationProperties;
import io.jsonwebtoken.*;
import javaslang.control.Option;
import javaslang.control.Try;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;
import java.util.List;

import static java.util.Objects.requireNonNull;

@Component
public class JwtTokenProvider {

    private final UserDetailsService userDetailsService;

    private final String secretKey;
    private final Long validityInMilliseconds;

    public JwtTokenProvider(final UserDetailsService userDetailsService,
                            final DetectionApiConfigurationProperties properties) {
        this.userDetailsService = userDetailsService;

        this.secretKey = Option.of(properties.getJwt())
                .flatMap(jwtConfig -> Option.of(jwtConfig.getTokenSecret()))
                .toTry(() -> new RuntimeException("jwt.token-secret is not present."))
                .flatMap(secret -> Try.of(() -> Base64.getEncoder().encodeToString(secret.getBytes())))
                .getOrElseThrow(() -> new RuntimeException("Could not encode provided token secret"));

        this.validityInMilliseconds = Option.of(properties.getJwt())
                .flatMap(jwtConfig -> Option.of(jwtConfig.getTokenExpire()))
                .toTry()
                .getOrElseThrow(() -> new RuntimeException("jwt.token-secret is not present."));
    }

    public Try<String> createToken(final String username,
                                   final List<String> roles) {
        return Try.of(() -> {

            final Claims claims = Jwts.claims().setSubject(username);
            claims.put("roles", roles);

            final Date now = new Date();
            final Date validity = new Date(now.getTime() + validityInMilliseconds);

            return Jwts.builder()
                    .setClaims(claims)
                    .setIssuedAt(now)
                    .setExpiration(validity)
                    .signWith(SignatureAlgorithm.HS256, secretKey)
                    .compact();
        });
    }

    public Authentication getAuthentication(final String token) {
        final UserDetails userDetails = this.userDetailsService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public String getUsername(final String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    public Try<Boolean> validateToken(final String token) {
        requireNonNull(token, "token is null");

        return Try
                .of(() -> {
                    final Jws<Claims> claims = Jwts.parser()
                            .setSigningKey(secretKey)
                            .parseClaimsJws(token);
                    final Date expiration = claims.getBody()
                            .getExpiration();

                    return !expiration.before(new Date());
                });
    }
}
