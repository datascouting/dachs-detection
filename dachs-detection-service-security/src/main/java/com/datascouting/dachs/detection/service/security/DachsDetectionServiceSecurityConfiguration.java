package com.datascouting.dachs.detection.service.security;

import com.datascouting.dachs.detection.service.config.DachsDetectionServiceConfigConfiguration;
import com.datascouting.dachs.detection.service.crud.DachsDetectionServiceCrudConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan
@Import({
        DachsDetectionServiceConfigConfiguration.class,
        DachsDetectionServiceCrudConfiguration.class,
})
public class DachsDetectionServiceSecurityConfiguration {
}
