package com.datascouting.dachs.detection.service.security.service;

import com.datascouting.dachs.detection.domain.entity.User;
import com.datascouting.dachs.detection.service.crud.system.user.UserCrudService;
import com.datascouting.dachs.detection.service.security.model.SecurityUser;
import javaslang.control.Try;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.function.Function;

import static java.util.Objects.requireNonNull;

@Service
public class SecurityUserDetailsService implements UserDetailsService {

    private final UserCrudService userCrudService;

    public SecurityUserDetailsService(UserCrudService userCrudService) {
        this.userCrudService = userCrudService;
    }

    public static Try<SecurityUser> userToSecurityUser(final User user) {
        requireNonNull(user, "user is null");

        return Try.of(() -> {
            final SecurityUser securityUser = new SecurityUser();
            BeanUtils.copyProperties(user, securityUser);

            return securityUser;
        });
    }

    public static Try<User> securityUserToUser(final SecurityUser securityUser) {
        requireNonNull(securityUser, "securityUser is null");

        return Try.of(() -> {
            final User user = new User();
            BeanUtils.copyProperties(securityUser, user);

            return user;
        });
    }

    @Override
    public SecurityUser loadUserByUsername(final String username) throws UsernameNotFoundException {
        return userCrudService
                .findByUsername(username, () -> new UsernameNotFoundException(username))
                .flatMap(SecurityUserDetailsService::userToSecurityUser)
                .getOrElseThrow((Function<Throwable, RuntimeException>) RuntimeException::new);
    }
}
